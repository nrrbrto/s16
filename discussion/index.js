
console.log("For Loop")

for(let num3 = 1; num3 <= 5; num3++){
for(let num3 = 1; num3 !== 6; num3++){
	console.log(num3);
}


for(let num5 = 1; num5 <= count2; num5++){
	console.log(num5);
}

console.log("String Iteration");

let myString = "Juan";
console.log(myString.length);

console.log(myString[0]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Displaying individual letters in a string")
// myString.lenght == 4
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

console.log("Displaying number 3 for vowels")

let myName = "AloNzO";

for(let i = 0; i < myName.length; i++){
	// if the character of the name is a vowel, instead of displaying the character, display number 3;
	if(
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

console.log("Continue and Break")

/*
	Continue and Break statements
	 - Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statents in a code block
	 - Break statement is used to terminate the current loop once a match has been found.
*/

for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count > 10) {
		break;
	}
}
